#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>
#include <string.h>
/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char *argv[]) {
	
	FILE *f = fopen("file.txt","w+");
	if(!f){
		printf("Couldn't open file. Code: %d\n'",strerror(errno));
	}
	srand(time(NULL));
	
	char *arrayOfNames[5] = {"Alpha","Beta","Charlie","Delta","Eta"};
	printf("%s\n",arrayOfNames[0]);
	for(int i = 0 ; i < 20; i++){
		fprintf(f,"%d. %s %d\n",i,arrayOfNames[i%5],rand()%100);
	}
	
	fpos_t pos;
	fgetpos(f,&pos);
	printf("Fpos: %d",pos);
	fseek(f,0,0);
	
	
	char buff;
	printf("\n");
	while((buff = fgetc(f)) != EOF){
		printf("%c",buff);
	}
	
	fseek(f,0,0);
	printf("Beginning of file: %d\n",ftell(f));
	
	fseek(f,0,2);
	printf("End of file: %d",ftell(f));
	
	fclose(f);
	
	return 0;
}
